    /**
     * Generic method for all applications to use for generating unique transaction refererences.
     */
    public static string generateUniqueTransactionReference(String application, String param) {
        Datetime dt = (System.now());
        string strYear = String.valueOf(dt.year());
        string strMonth = String.valueOf(dt.month());
        string strDay = String.valueOf(dt.day());
        string strHour = String.valueOf(dt.hour());
        string strMinute = String.valueOf(dt.minute());
        string strSecond = String.valueOf(dt.second());

        if (strMonth.length() == 1) strMonth = '0' + strMonth;
        if (strDay.length() == 1) strDay = '0' + strDay;
        if (strHour.length() == 1) strHour = '0' + strHour;
        if (strMinute.length() == 1) strMinute = '0' + strMinute;
        if (strSecond.length() == 1) strSecond = '0' + strSecond;

        string currentDateFormatted = strYear + '-' + strMonth + '-' + strDay;
        string currentDateTimeUnFormatted = strYear + strMonth + strDay + strHour + strMinute + strSecond;

        String externalTransactionReference = application + param + currentDateTimeUnFormatted;
        return externalTransactionReference;
    }